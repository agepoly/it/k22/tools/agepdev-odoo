# AGEPDev Odoo

AGEPDev images are Docker images for in-cluster development at AGEPoly.
This image creates a simple development environment for Odoo.