FROM odoo:14.0
# Docker base: odoo:14.0 @ https://github.com/odoo/docker/blob/3864188c94c33dace7d4b5f6767e00a4328fe08c/14.0/Dockerfile
# Production Dockerfile: https://gitlab.com/agepoly/it/dev/odoo/-/blob/main/Dockerfile?ref_type=heads

# Revert base image user and entrypoint
USER root

######
### s6-overlay installation
### https://github.com/just-containers/s6-overlay#installation
######

# set version for s6 overlay
ARG S6_OVERLAY_VERSION="3.1.5.0"
ARG S6_OVERLAY_ARCH="x86_64"

# add s6 overlay
ADD https://github.com/just-containers/s6-overlay/releases/download/v${S6_OVERLAY_VERSION}/s6-overlay-noarch.tar.xz /tmp
RUN tar -C / -Jxpf /tmp/s6-overlay-noarch.tar.xz
ADD https://github.com/just-containers/s6-overlay/releases/download/v${S6_OVERLAY_VERSION}/s6-overlay-${S6_OVERLAY_ARCH}.tar.xz /tmp
RUN tar -C / -Jxpf /tmp/s6-overlay-${S6_OVERLAY_ARCH}.tar.xz

# add s6 optional symlinks
ADD https://github.com/just-containers/s6-overlay/releases/download/v${S6_OVERLAY_VERSION}/s6-overlay-symlinks-noarch.tar.xz /tmp
RUN tar -C / -Jxpf /tmp/s6-overlay-symlinks-noarch.tar.xz
ADD https://github.com/just-containers/s6-overlay/releases/download/v${S6_OVERLAY_VERSION}/s6-overlay-symlinks-arch.tar.xz /tmp
RUN tar -C / -Jxpf /tmp/s6-overlay-symlinks-arch.tar.xz

# copy s6 configuration
COPY etc/ /etc/

ENTRYPOINT ["/init"]
CMD []

######
### code-server installation
### https://github.com/coder/code-server/blob/main/ci/release-image/Dockerfile
######

RUN \
  apt update && \
  apt install -y curl zsh git vim htop sudo netcat && \
  echo "agepinfo ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers.d/nopasswd

ARG CODE_SERVER_VERSION="4.18.0"

RUN \
  curl -fOL https://github.com/coder/code-server/releases/download/v$CODE_SERVER_VERSION/code-server_${CODE_SERVER_VERSION}_amd64.deb && \
  dpkg -i code-server_${CODE_SERVER_VERSION}_amd64.deb && \
  useradd -u 1000 -U -m agepinfo && \
  usermod -G users agepinfo && \
  chsh -s /bin/bash agepinfo

ENV HOME="/home/agepinfo/dev-storage/"

# code-server default http port
EXPOSE 8443